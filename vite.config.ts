import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import { VitePWA } from 'vite-plugin-pwa'
import { identity } from 'svelte/internal'

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  plugins: [
    svelte(),
    VitePWA({
      registerType: 'autoUpdate',
      includeAssets: ['icons/icon.svg', 'icons/icon-192.png', 'icons/icon-512.png', 'icons/apple-touch-icon.png', 'icons/masked-icon.svg'],
      manifest: {
        name: 'Apkes',
        short_name: 'Apkes',
        description: 'Simple health app.',
        lang: 'en',
        theme_color: '#064e3b',
        background_color: '#064e3b',
        display: 'standalone',
        icons: [
          {
            src: 'icons/icon-192.png',
            sizes: '192x192',
            type: 'image/png'
          },
          {
            src: 'icons/icon-512.png',
            sizes: '512x512',
            type: 'image/png'
          },
          {
            src: 'icons/masked-icon.svg',
            sizes: '120x120',
            type: 'image/svg+xml'
          }
        ]
      }
    })
  ]
})
