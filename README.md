<p style="text-align:center">
    <img style="align:center" alt="Apkes icon" src="public/icons/icon.svg">
</p>

<h1 style="text-align:center">Apkes</h1>

[![Translation status](https://translate.codeberg.org/widgets/apkes/-/svg-badge.svg)](https://translate.codeberg.org/engage/apkes/)

<p style="text-align:center"><a href="https://codeberg.org/Linerly/apkes/src/branch/main/README-id.md">Klik di sini untuk membaca README ini dalam bahasa Indonesia</a></p>

[![Translation status](https://translate.codeberg.org/widgets/apkes/-/svg-badge.svg)](https://translate.codeberg.org/engage/apkes/)

<p style="text-align:center"><a href="https://codeberg.org/Linerly/apkes/src/branch/main/README-nl.md">Lees deze informatie in het Nederlands</a></p>

# *Simple* health app — https://apkes.linerly.xyz

## Is this an actual health app?
Not exactly. This is just a simple web app that has quotes, suggestions, and a section where you can reflect the things happening today — for example, your mood, activities, etc.

These things *can* make living a healthy life easy though. :)

## Will you add more things into the app?
I could add more things if I want to. Besides, this web app is for my school project, and the project doesn't have to end there.

## How many languages does the app have?
Initially, the app only has Indonesian and English. Feel free to translate the app into your native language on Weblate!

[![Translation status](https://translate.codeberg.org/widgets/apkes/-/multi-auto.svg)](https://translate.codeberg.org/engage/apkes/)