const config = {
  content: [
      "./src/**/*.{svelte,ts}",
      "./index.html"
  ],

  theme: {
    extend: {},
  },

  plugins: [],
};

module.exports = config;