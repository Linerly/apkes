<p style="text-align:center">
    <img style="align:center" alt="Apkes icon" src="public/icons/icon.svg">
</p>

<h1 style="text-align:center">Apkes</h1>

[![Translation status](https://translate.codeberg.org/widgets/apkes/-/svg-badge.svg)](https://translate.codeberg.org/engage/apkes/)

# *Eenvoudige* gezondheidsapp — https://apkes.linerly.xyz

## Is dit een échte gezondheidsapp?
Nee. Dit is slechts een eenvoudige webapp met citaten, suggesties en een sectie met zelfreflectie, zoals je stemming, activiteiten, etc.

Uiteraard kan deze app wél bĳdragen aan een gezondere leefstĳl. :)

## Kun je meer functies toevoegen aan de app?
Dat kan, tenminste, als ik dat zou willen. Deze webapp is eigenlĳk bedoeld als schoolproject, maar de ontwikkeling kan ook ná schooltĳd doorgaan.

## Hoeveel talen zĳn er beschikbaar?
De app was in eerste instantie alleen beschikbaar in het Engels en Indonesisch. Als je wilt, kun je helpen om de app te vertalen op Weblate!

[![Translation status](https://translate.codeberg.org/widgets/apkes/-/multi-auto.svg)](https://translate.codeberg.org/engage/apkes/)
