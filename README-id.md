<p style="text-align:center">
    <img style="align:center" alt="Ikon Apkes" src="public/icons/icon.svg">
</p>

<h1 style="text-align:center">Apkes</h1>

[![Status penerjemahan](https://translate.codeberg.org/widgets/apkes/-/svg-badge.svg)](https://translate.codeberg.org/engage/apkes/)

<p style="text-align:center"><a href="https://codeberg.org/Linerly/apkes/src/branch/main/README.md">Click here to read this README in English</a></p>

# Aplikasi kesehatan *sederhana* — https://apkes.linerly.xyz

## Apakah ini memang sebuah aplikasi kesehatan?
Tidak terlalu. Ini hanya aplikasi kesehatan yang sederhana yang memiliki kutipan, saran, dan bagian di mana Anda dapat merefleksikan hal-hal yang terjadi hari ini — misalnya, perasaan Anda, aktivitas yang dilakukan, dan lain-lain.

Cara-cara ini *bisa saja* membuat hidup sehat mudah. :)

## Apakah aplikasinya nanti ditambahkan  hal-hal baru?
Saya bisa jika saya ingin. Meskipun aplikasi web ini untuk proyek sekolah, proyeknya tidak harus berakhir di situ.

## Berapa banyak bahasa yang aplikasi ini punya?
Awalnya, hanya ada bahasa Indonesia dan Inggris. Silakan terjemahkan aplikasi ini ke bahasa Anda di Weblate!

[![Status penerjemahan](https://translate.codeberg.org/widgets/apkes/-/multi-auto.svg)](https://translate.codeberg.org/engage/apkes/)